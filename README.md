**Tehnolgije za izradu aplikacije:**

Node.js --v14.8.0.

MongoDB --- v3.4.24 Enterprise Server

**Tehnologije za testiranje aplikacije:**

Zed Attack Proxy--- v.2.9.0 --testiranje sigurnosti

SoapSonar Personal -v. 9 ---testiranje funkcionalnosti

**Pristupanje web aplikaciji preko url:** http://nadzornisustav.hopto.org:3000/login 

Admin---> username:admin  password:admin

User---> username:user    password:user

**URL aplikacije**--- https://gitlab.com/kristijan_mifka/video-nadzor-app

**URL baze podataka** --- https://gitlab.com/kristijan_mifka/video-nadzor-app/-/tree/APP/DBMS

**Unutar url**: https://gitlab.com/kristijan_mifka/video-nadzor-app/-/blob/APP/Projektna_dokumentacija-Amel_Mujki%C4%87-Kristijan_Mifka.docx postavljena Korisnička dokumentacija za instalaciju i korištenje - koraci- str. 57-61

**Dokumentacija word**: https://gitlab.com/kristijan_mifka/video-nadzor-app/-/blob/APP/Projektna_dokumentacija-Amel_Mujki%C4%87-Kristijan_Mifka.docx

**Dokumentracija pdf**: https://gitlab.com/kristijan_mifka/video-nadzor-app/-/blob/APP/Projektna_dokumentacija-Amel_Mujki%C4%87-Kristijan_Mifka.pdf

**Kritički ostvrt Mifka**: https://gitlab.com/kristijan_mifka/video-nadzor-app/-/blob/APP/PI-_Kriti%C4%8Dki_osvrti_Mifka.docx

**Kritički ostvrt Mujkić**:  https://gitlab.com/kristijan_mifka/video-nadzor-app/-/blob/APP/PI_Kriticki_osvrt-Amel_Mujki%C4%87.docx