const Koa = require("koa");
const KoaRouter = require("koa-router");
const KoaStatic = require("koa-static");
const KoaSession = require("koa-session");
const passport = require("koa-passport");
const bodyParser = require("koa-bodyparser");
const json = require("koa-json");
const render = require("koa-ejs");
const path = require("path");
const fs = require('fs');
const mongoose = require("mongoose");
const moment = require("moment");
const { execSync } = require("child_process");

const { User, Ticket } = require("./schema");

const app = new Koa();
const router = new KoaRouter();
const appName = 'nadzorni-sustav';
const results = Object.create(null); // or just '{}', an empty object


// Passport config
require("./config/passport")(passport);

// Middlewares
app.keys = ["secret"];
app.use(json());
app.use(bodyParser());
app.use(KoaSession({}, app));
app.use(KoaStatic(path.join(__dirname, "style")));
app.use(KoaStatic(path.join(__dirname, "static")));
app.use(passport.initialize());
app.use(passport.session());

// DB Connection
mongoose
  .connect(
    "mongodb://superadmin:using-bias-tiptoe1@ds147487.mlab.com:47487/nadzorni-sustav",
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
  .then(() => console.log(`${appName}: Spojen na DB!`))
  .catch(() => console.error.bind(console, `${appName}: Greška pri spajanju na DB:`));

router.get("/", async (ctx) => {
  if (ctx.isAuthenticated()) {
const hostname = execSync("curl ifconfig.me");
    await ctx.render("index", {
      user: ctx.state.user,
      hostname,
      isLogin: false,
    });
} else {
    ctx.redirect("/login");
  }

});

router.get("/login", async (ctx) => {
  if (!ctx.isAuthenticated()) {
    await ctx.render("login", {
      user: ctx.state.user,
      isLogin: true,
    });
  } else {
    ctx.redirect("/");
  }
});

router.get("/all", async (ctx) => {
  if (ctx.isAuthenticated()) {
    if (ctx.state.user.role === "admin") {
      const videos = await new Promise((resolve, reject) => {
	return fs.readdir('/nginx/stream_recordings/', (err, files) => {
          if (err) {
            reject(err);
          }
          const filesSorted = files
		.map(item => ({name: item, epoch: item.split('-')[1]}))
		.sort((a, b) => b.epoch - a.epoch);
          resolve(filesSorted);
      	  });
	});
const hostname = execSync("curl ifconfig.me");
	 await ctx.render("all", {
          videos,
	  hostname,
          user: ctx.state.user,
          isLogin: false,
        });
    } else {
      ctx.redirect("/");
    }
  } else {
    ctx.redirect("/login");
  }
});

router.post("/login", async (ctx) => {
  return passport.authenticate("local", (err, user, info, status) => {
    if (!user) {
      ctx.status = 401;
      ctx.body = info;
    } else {
      ctx.login(user).then(() => {
        console.log("User:", user.username, "logged in.");
        ctx.login(user);
        ctx.redirect("/");
      });
    }
    console.log(err, user, info, status);
  })(ctx);
});

router.get("/logout", async (ctx) => {
  ctx.logout();
  ctx.redirect("/login");
});

router.delete("/video/:name", async (ctx) => {
  const videoName = ctx.params.name;
  const data = new Promise((resolve, reject) => {
	  return fs.unlink(`/nginx/stream_recordings/${videoName}`, err => err ? reject(err) : resolve(videoName))});
  console.log(`Izbrisan video! \n ${videoName}`);
  ctx.body = { success: true };
  ctx.redirect("/");
});

render(app, {
  root: path.join(__dirname, "views"),
  layout: "layout",
  viewExt: "html",
  cache: false,
  debug: false,
});

app.use(router.routes()).use(router.allowedMethods());

app.listen(3000, () => console.log(`${appName}: API pokrenut!`));


const { networkInterfaces } = require('os');

const nets = networkInterfaces();

for (const name of Object.keys(nets)) {
    for (const net of nets[name]) {
        // skip over non-ipv4 and internal (i.e. 127.0.0.1) addresses
        if (net.family === 'IPv4' && !net.internal) {
            if (!results[name]) {
                results[name] = [];
            }

            results[name].push(net.address);
        }
    }
}
